<?php

use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use GuzzleHttp\Client as GuzzleHttpClient;

require_once 'vendor/autoload.php';
require_once 'inc/config.inc.php';


/**
 * Création du fichier de Log.
 */
$logger = new Logger('logger');
$logger->pushHandler(new StreamHandler(FICHIER_LOG));

/**
 * Interogger le script prsent sur ecfweb
 */
$http_client = new GuzzleHttpClient([
    'base_uri' => MAPA_SRV,
    'verify' => false
]);

$url = MAPA_SRV."cron_chomette.php";

try {
    $response = $http_client->request('GET', $url, [ ]);
} catch (\Exception $ex) {
    $logger->error(sprintf('Run WaitingOrder exception : commande "%s" message "%s"', $order_id, $ex->getMessage()));
}
