<?php

use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use GuzzleHttp\Client as GuzzleHttpClient;

require_once 'vendor/autoload.php';
require_once 'inc/config.inc.php';


/**
 * Création du fichier de Log.
 */
$logger = new Logger('logger');
$logger->pushHandler(new StreamHandler(FICHIER_LOG));

/**
 * Interogger le script prsent sur ecfweb
 */
$http_client = new GuzzleHttpClient([
    'base_uri' => MAPA_SRV,
    'verify' => false
]);

$url = MAPA_SRV."ws_tracking.php";


/**
 * Recherche des trackings 
 */
$ApiOrderTracking = new ApiOrderTracking('MAPA');
$ApiOrderTracking->associateOrder();
$OrderTrackings = $ApiOrderTracking->searchTracking();

foreach ($OrderTrackings as $key => $anOrderTracking) {

    $products = $ApiOrderTracking->getArticles($anOrderTracking["ORDER_ID"]);

    try {
        $response = $http_client->request('POST', $url."?account=chomette", [
            'headers' => [
                'Content-Type' => 'application/json'
            ],
            'body' => json_encode([
                'orderId' => $anOrderTracking["ORDER_ID"],
                'tracking' => [
                    "comments" => $anOrderTracking["PRT_URLSUIVI"],
                    "trackign_number" => $anOrderTracking["C0CKNA"],
                    "products" => $products
                ]

            ])
        ]);
        $code = $response->getStatusCode();
        $body = $response->getBody()->getContents();
        if ($code === 200) {
            echo $body;
            $ApiOrderTracking->setTracking($anOrderTracking["ORDER_ID"]);
        }
    } catch (\Exception $ex) {
        $logger->error(sprintf('Send Tracking exception : commande "%s" message "%s"', $anOrderTracking["ORDER_ID"], $ex->getMessage()));
    }
}


