<?php

use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use GuzzleHttp\Client as GuzzleHttpClient;

require_once 'vendor/autoload.php';
require_once 'inc/config.inc.php';


/**
 * Création du fichier de Log.
 */
$logger = new Logger('logger');
$logger->pushHandler(new StreamHandler(FICHIER_LOG));

/**
 * Interogger le script prsent sur ecfweb
 */
$http_client = new GuzzleHttpClient([
    'base_uri' => MAPA_SRV,
    'verify' => false
]);

$url = MAPA_SRV."ws_InvoiceData.php";


/**
 * Recherche des trackings 
 */
$ApiInvoice = new ApiInvoice();

$Invoices = $ApiInvoice->searchInvoice();

foreach ($Invoices as $key => $anInvoice) {
    try {
        $response = $http_client->request('POST', $url."?account=chomette", [
            'headers' => [
                'Content-Type' => 'application/json'
            ],
            'body' => json_encode([
                'orderId' => trim($anInvoice["ID_COMMANDE_MAPA"]),
                'invoice' => [
                    "invoiceNumber" => trim($anInvoice["N_FACTURE"]),
                    "invoiceDate" => date('c')
                ]
            ])
        ]);
        $code = $response->getStatusCode();
        if ($code === 200) {
            $ApiInvoice->setStatus($anInvoice["ID_COMMANDE_MAPA"]);
        }
    } catch (\Exception $ex) {
        $logger->error(sprintf('Send Tracking exception : commande "%s" message "%s"', $anInvoice["ID_COMMANDE_MAPA"], $ex->getMessage()));
    }
}


