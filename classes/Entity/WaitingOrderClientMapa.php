<?php

class WaitingOrderClientMapa
{
    private $MWO_ORDERID ;
    private $MWO_CODE_SOCIETE ;
    private $MWO_SIRET ;
    private $MWO_CHORUS_SERVICE ;
    private $MWO_DATE_COMMANDE ;
    private $MWO_CDE_JSON ;

    public function __construct($MWO_ORDERID = null, $MWO_CODE_SOCIETE = null, $MWO_SIRET = null, $MWO_CHORUS_SERVICE = null, $MWO_DATE_COMMANDE = null, $MWO_CDE_JSON = null){
        $this->MWO_ORDERID = $MWO_ORDERID;
        $this->MWO_CODE_SOCIETE = $MWO_CODE_SOCIETE;
        $this->MWO_SIRET = $MWO_SIRET;
        $this->MWO_CHORUS_SERVICE = $MWO_CHORUS_SERVICE;
        $this->MWO_DATE_COMMANDE = $MWO_DATE_COMMANDE;
        $this->MWO_CDE_JSON = $MWO_CDE_JSON;
    }

    public function save(){

        if (WaitingOrderClientMapaRepository::existWaitingOrder($this->MWO_ORDERID)) {
            $verif = "La commande est déjà présente dans waiting order.";
        }else{
            $verif = WaitingOrderClientMapaRepository::add($this->MWO_ORDERID,$this->MWO_CODE_SOCIETE,$this->MWO_SIRET,$this->MWO_CHORUS_SERVICE,$this->MWO_DATE_COMMANDE,$this->MWO_CDE_JSON);
            if ($verif[0]) {
                $verif[2] = "La commande a été ajoute au waiting order.";
            }else{
                $verif[2] = "Erreur lors de l'ajout.";
            }
            
        }

        return $verif; 
        
    }

    public function delete(){

        if (WaitingOrderClientMapaRepository::existWaitingOrder($this->MWO_ORDERID)) {
            $verif = WaitingOrderClientMapaRepository::delete($this->MWO_ORDERID,$this->MWO_CODE_SOCIETE,$this->MWO_SIRET,$this->MWO_CHORUS_SERVICE,$this->MWO_DATE_COMMANDE,$this->MWO_CDE_JSON);
            if ($verif[0]) {
                $verif[2] = "La commande a été supprimer du waiting order.";
            }else{
                $verif[2] = "Erreur lors de la supression.";
            }
            
        }else{
            $verif = "La commande n'existe pas dans waiting order.";
        }

        return $verif; 
        
    }

    public function loadWaitingOrders(){
        $tableauWaitingOrders = WaitingOrderClientMapaRepository::findAll();
        return $tableauWaitingOrders;
    }

    public function getMWO_ORDERID() { return $this->MWO_ORDERID ; }
    public function getMWO_CODE_SOCIETE() { return $this->MWO_CODE_SOCIETE ; }
    public function getMWO_SIRET() { return $this->MWO_SIRET ; }
    public function getMWO_CHORUS_SERVICE() { return $this->MWO_CHORUS_SERVICE ; }
    public function getMWO_DATE_COMMANDE() { return $this->MWO_DATE_COMMANDE ; }
    public function getMWO_CDE_JSON() { return $this->MWO_CDE_JSON ; }

    public function setMWO_ORDERID( $MWO_ORDERID ) { $this->MWO_ORDERID = $MWO_ORDERID ;}
    public function setMWO_CODE_SOCIETE( $MWO_CODE_SOCIETE ) { $this->MWO_CODE_SOCIETE = $MWO_CODE_SOCIETE ;}
    public function setMWO_SIRET( $MWO_SIRET ) { $this->MWO_SIRET = $MWO_SIRET ;}
    public function setMWO_CHORUS_SERVICE( $MWO_CHORUS_SERVICE ) { $this->MWO_CHORUS_SERVICE = $MWO_CHORUS_SERVICE ;}
    public function setMWO_DATE_COMMANDE( $MWO_DATE_COMMANDE ) { $this->MWO_DATE_COMMANDE = $MWO_DATE_COMMANDE ;}
    public function setMWO_CDE_JSON( $MWO_CDE_JSON ) { $this->MWO_CDE_JSON = $MWO_CDE_JSON ;}
}