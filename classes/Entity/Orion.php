<?php
use Monolog\Handler\StreamHandler;
use Monolog\Logger;

class Orion
{   
    private  $host = HOST;
    private  $user = USERNAME;
    private  $mdp = PASSWORD;
    private  $connexion = null;

    public function __construct() {
        try {
            $this->connexion = db2_connect($this->host, $this->user,$this->mdp);
        } catch (\Exception $ex) {
            $logger = new Logger('logger');
            $logger->pushHandler(new StreamHandler(FICHIER_LOG));
            $logger->info( 'Erreur Connexion BDD.');
            $logger->info($ex->getMessage());
        }
        
    }

    public function __destruct() {
        db2_close($this->connexion);
        $this->connexion = null;
    }

    public function getConnexion(){
        return $this->connexion;
    }
}
