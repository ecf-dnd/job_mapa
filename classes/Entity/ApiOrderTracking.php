<?php

class ApiOrderTracking
{
    private $API_CODE ; 
    private $ORDER_ID ; 
    private $CUSTOMER_ID ; 
    private $ORDER_NUM ; 
    private $CUSTOMER_NUM ; 
    private $DATE_CREATION ; 
    private $HOUR_CREATION ; 
    private $UPDATEDATE ; 
    private $ORDER_STATUS ; 
    private $SHIPMENT_NOTIF_SENT ; 

    public function __construct( $API_CODE = null, $ORDER_ID = null, $CUSTOMER_ID = null, $CUSTOMER_NUM = null ){
        $this->API_CODE = strtoupper($API_CODE);
        $this->ORDER_ID = $ORDER_ID;
        $this->CUSTOMER_ID = $CUSTOMER_ID;
        $this->CUSTOMER_NUM = $CUSTOMER_NUM;
    }

    public function save(){
        if (ApiOrderTrackingRepository::existApiOrderTracking($this->API_CODE, $this->ORDER_ID, $this->CUSTOMER_ID)) {
            $verif = "Le tracking existe déjà dans API_ORDER_TRACKING.";
        }else{
            $verif = ApiOrderTrackingRepository::add($this->API_CODE, $this->ORDER_ID, $this->CUSTOMER_ID, $this->CUSTOMER_NUM);
            if ($verif[0]) {
                $verif[2] = "Le tracking a été ajoute à API_ORDER_TRACKING.";
            }else{
                $verif[2] = "Erreur lors de l'ajout.";
            }
        }
        return $verif; 
    }

    public function associateOrder(){
        ApiOrderTrackingRepository::associateOrderIdWithOrionId($this->API_CODE);
    }

    public function getArticles($ORDER_ID){
        $Articles = ApiOrderTrackingRepository::findArticlesByOrderId($ORDER_ID);
        return $Articles ; 
    }

    public function searchTracking(){
        $OrderTracking = ApiOrderTrackingRepository::searchShippmentTracking();
        return $OrderTracking;
    }
    public function setTracking($ORDER_ID){
        $OrderTracking = ApiOrderTrackingRepository::update($ORDER_ID);
        return $OrderTracking;
    }

    public function getAPI_CODE(){return $this->API_CODE;}
    public function getORDER_ID(){return $this->ORDER_ID;}
    public function getCUSTOMER_ID(){return $this->CUSTOMER_ID;}
    public function getORDER_NUM(){return $this->ORDER_NUM;}
    public function getCUSTOMER_NUM(){return $this->CUSTOMER_NUM;}
    public function getDATE_CREATION(){return $this->DATE_CREATION;}
    public function getHOUR_CREATION(){return $this->HOUR_CREATION;}
    public function getUPDATEDATE(){return $this->UPDATEDATE;}
    public function getORDER_STATUS(){return $this->ORDER_STATUS;}
    public function getSHIPMENT_NOTIF_SENT(){return $this->SHIPMENT_NOTIF_SENT;}

    public function setAPI_CODE($API_CODE) {$this->API_CODE = strtoupper($API_CODE);}
    public function setORDER_ID($ORDER_ID) {$this->ORDER_ID = $ORDER_ID;}
    public function setCUSTOMER_ID($CUSTOMER_ID) {$this->CUSTOMER_ID = $CUSTOMER_ID;}
    public function setORDER_NUM($ORDER_NUM) {$this->ORDER_NUM = $ORDER_NUM;}
    public function setCUSTOMER_NUM($CUSTOMER_NUM) {$this->CUSTOMER_NUM = $CUSTOMER_NUM;}
    public function setDATE_CREATION($DATE_CREATION) {$this->DATE_CREATION = $DATE_CREATION;}
    public function setHOUR_CREATION($HOUR_CREATION) {$this->HOUR_CREATION = $HOUR_CREATION;}
    public function setUPDATEDATE($UPDATEDATE) {$this->UPDATEDATE = $UPDATEDATE;}
    public function setORDER_STATUS($ORDER_STATUS) {$this->ORDER_STATUS = $ORDER_STATUS;}
    public function setSHIPMENT_NOTIF_SENT($SHIPMENT_NOTIF_SENT) {$this->SHIPMENT_NOTIF_SENT = $SHIPMENT_NOTIF_SENT;}

}