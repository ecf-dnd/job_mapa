<?php

class ApiInvoice
{
    private $API_CODE ; 
    private $ORDER_ID ; 
    private $CUSTOMER_ID ; 


    public function __construct( $API_CODE = null, $ORDER_ID = null, $CUSTOMER_ID = null){
        $this->API_CODE = strtolower($API_CODE);
        $this->ORDER_ID = $ORDER_ID;
        $this->CUSTOMER_ID = $CUSTOMER_ID;
    }

    public function searchInvoice(){
        $InvoiceOrder = ApiInvoiceRepository::searchInvoiceOfOrder();
        return $InvoiceOrder;
    }
    public function setStatus($ORDER_ID){
        $InvoiceOrder = ApiInvoiceRepository::update($ORDER_ID);
        return $InvoiceOrder;
    }

    public function getAPI_CODE(){return $this->API_CODE;}
    public function getORDER_ID(){return $this->ORDER_ID;}
    public function getCUSTOMER_ID(){return $this->CUSTOMER_ID;}
   
    public function setAPI_CODE($API_CODE) {$this->API_CODE = strtolower($API_CODE);}
    public function setORDER_ID($ORDER_ID) {$this->ORDER_ID = $ORDER_ID;}
    public function setCUSTOMER_ID($CUSTOMER_ID) {$this->CUSTOMER_ID = $CUSTOMER_ID;}
   

}