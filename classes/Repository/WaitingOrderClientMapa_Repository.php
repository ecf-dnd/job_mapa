<?php

use Monolog\Handler\StreamHandler;
use Monolog\Logger;

class WaitingOrderClientMapaRepository
{
    public static function findAll(){
        $Orion = new Orion();
        $connexion = $Orion->getConnexion();
        $req  = "SELECT MWO_ORDERID, MWO_CODE_SOCIETE, MWO_SIRET, MWO_CHORUS_SERVICE, MWO_DATE_COMMANDE, MWO_CDE_BLOB FROM orionbd.mapa_waiting_order";
        $res = db2_prepare($connexion, $req);
        $WaitingOrder = [];
        if (db2_execute($res,array())) {
            /**
             * Fonction db2_fetch_assoc ne fonctionne pas
             */
            while (db2_fetch_row($res)) {
                $Ligne["MWO_ORDERID"] = db2_result($res, 0);
                $Ligne["MWO_CODE_SOCIETE"] = db2_result($res, 1);
                $Ligne["MWO_SIRET"] = db2_result($res, 2);
                $Ligne["MWO_CHORUS_SERVICE"] = db2_result($res, 3);
                $Ligne["MWO_DATE_COMMANDE"] = db2_result($res, 4);
                $Blob = db2_result($res, 5);
                $Ligne["MWO_CDE_BLOB"] = gzdecode( $Blob );
                array_push($WaitingOrder, $Ligne);
            }
        }
        return $WaitingOrder;
    }
    
    public static function existWaitingOrder($MWO_ORDERID){
        $Orion = new Orion();
        $connexion = $Orion->getConnexion();
        $req  = "SELECT MWO_ORDERID FROM orionbd.mapa_waiting_order WHERE MWO_ORDERID = ?";
        $res = db2_prepare($connexion, $req);
        //$Orion->__destruct();
        $WaitingOrder = FALSE;
        if (db2_execute($res,array($MWO_ORDERID))) {
            while ($row = db2_fetch_assoc($res)) {
                $WaitingOrder = $row;
            }
        }
        return $WaitingOrder;
    }

    public static function add($MWO_ORDERID, $MWO_CODE_SOCIETE, $MWO_SIRET, $MWO_CHORUS_SERVICE, $MWO_DATE_COMMANDE, $MWO_CDE_JSON){
        //$retour = true;
            $Orion = new Orion();
            $connexion = $Orion->getConnexion();
            $req = "INSERT INTO orionbd.mapa_waiting_order (MWO_ORDERID, MWO_CODE_SOCIETE, MWO_SIRET, MWO_CHORUS_SERVICE, MWO_DATE_COMMANDE, MWO_CDE_BLOB) VALUES ( ? , ? , ? , ? , ? , ?)";
            $stmt = db2_prepare($connexion, $req);

            $json = json_encode($MWO_CDE_JSON);
            $retour[0] = db2_execute($stmt, array( $MWO_ORDERID, $MWO_CODE_SOCIETE, $MWO_SIRET, $MWO_CHORUS_SERVICE, $MWO_DATE_COMMANDE, gzencode($json) ));
            $retour[1]["JSON"] = $json ; 
            $retour[1]["Erreur"] = db2_stmt_error($stmt);
            $retour[1]["ErreurMsg"] =db2_stmt_errormsg ($stmt);
            //$Orion->__destruct();

        return $retour; 
    }

    public static function delete($MWO_ORDERID){
        //$retour = true;
        $Orion = new Orion();
        $connexion = $Orion->getConnexion();
        $req = "DELETE FROM orionbd.mapa_waiting_order  WHERE MWO_ORDERID = ? ";
        $stmt = db2_prepare($connexion, $req);

        $retour[0] = db2_execute($stmt, array( $MWO_ORDERID) ); 
        $retour[1]["Erreur"] = db2_stmt_error($stmt);
        $retour[1]["ErreurMsg"] =db2_stmt_errormsg ($stmt);
        //$Orion->__destruct();

        return $retour; 
    }


}