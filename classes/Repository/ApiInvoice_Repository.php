<?php

use Monolog\Handler\StreamHandler;
use Monolog\Logger;

class ApiInvoiceRepository
{
    public static function searchInvoiceOfOrder(){
        $Orion = new Orion();
        $connexion = $Orion->getConnexion();
        $req  = "SELECT ORDER_ID as ID_COMMANDE_MAPA,  ORDER_NUM as ID_COMMANDE_GESCOM , B6BTCD as N_FACTURE 
                 FROM orionbd.API_ORDER_TRACKING 
                 INNER JOIN orionbd.ORB6REP ON ORDER_NUM = B6BPCD AND CUSTOMER_NUM = B6AZCD
                 WHERE API_CODE = 'MAPA' AND ORDER_STATUS = 2 ; ";
        $res = db2_prepare($connexion, $req);
        $ApiOrderTrackin = [];
        if (db2_execute($res,array())) {
            while ($row = db2_fetch_assoc($res)) {
                array_push($ApiOrderTrackin, $row);
            }
        }
        return $ApiOrderTrackin;
    }

    public static function update($ORDER_ID){
        $Orion = new Orion();
        $connexion = $Orion->getConnexion();
        $sql_update = "UPDATE orionbd.API_ORDER_TRACKING set ORDER_STATUS = ? , UPDATEDATE = ? where API_CODE='MAPA' and ORDER_ID= ?";
        $res = db2_prepare($connexion, $sql_update);
        $update = FALSE;
        if (db2_execute($res, array(3 , "1". date('ymd') ,  $ORDER_ID  ))) {
            $update = TRUE;
        }
        return $update;
    }

}