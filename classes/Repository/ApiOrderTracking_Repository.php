<?php

use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use GuzzleHttp\Client as GuzzleHttpClient;


class ApiOrderTrackingRepository
{
    public static function existApiOrderTracking($API_CODE, $ORDER_ID, $CUSTOMER_ID){
        $Orion = new Orion();
        $connexion = $Orion->getConnexion();
        $req  = "SELECT API_CODE , ORDER_ID , CUSTOMER_ID FROM orionbd.API_ORDER_TRACKING WHERE API_CODE = ? AND ORDER_ID = ? AND CUSTOMER_ID = ? ";
        $res = db2_prepare($connexion, $req);
        $ApiOrderTrackin = (db2_execute($res, array($API_CODE, $ORDER_ID, $CUSTOMER_ID))) ? db2_fetch_assoc($res) : FALSE;
        //$Orion->__destruct();
        return $ApiOrderTrackin;
    }

    public static function add($API_CODE, $ORDER_ID, $CUSTOMER_ID, $CUSTOMER_NUM){
        //$retour = true;
        $Orion = new Orion();
        $connexion = $Orion->getConnexion();
        $req = "INSERT INTO orionbd.API_ORDER_TRACKING ( API_CODE , ORDER_ID , CUSTOMER_ID , ORDER_NUM , CUSTOMER_NUM , DATE_CREATION , HOUR_CREATION , UPDATEDATE , ORDER_STATUS , SHIPMENT_NOTIF_SENT ) VALUES (?,?,?,?,?,?,?,?,?,?)";
        $stmt = db2_prepare($connexion, $req);

        $retour[0] = db2_execute($stmt, array( $API_CODE, $ORDER_ID, $CUSTOMER_ID, 0, $CUSTOMER_NUM, "1". date('ymd') , date('His'), 0, '1', 0));
        $retour[1]["Erreur"] = db2_stmt_error($stmt);
        $retour[1]["ErreurMsg"] =db2_stmt_errormsg ($stmt);
        //$Orion->__destruct();

        return $retour;
    }

    public static function associateOrderIdWithOrionId($API_CODE)
    {
        $Orion = new Orion();
        $connexion = $Orion->getConnexion();
        $sql_search = "SELECT  API_CODE  , ORDER_ID , CUSTOMER_ID , CEAZN6 as NUM_ORDER
                        FROM orionbd.API_ORDER_TRACKING
                        INNER JOIN orionbd.ecfeicde ON cenlot = concat( ? , trim(ORDER_ID) ) AND CEAZN6 NOT IN ( '' , '9999999' )
                        WHERE API_CODE = ? AND ORDER_NUM = 0 ";
        $stmt_search = db2_prepare($connexion, $sql_search);
        if ($stmt_search) {
            $sql_update = "UPDATE orionbd.API_ORDER_TRACKING SET ORDER_NUM = ? , UPDATEDATE = ? WHERE API_CODE=? AND ORDER_ID= ?";
            $stmt_update = db2_prepare($connexion, $sql_update);
            if ($stmt_update) {
                $result = db2_execute($stmt_search,array($API_CODE,$API_CODE));
                while ($row = db2_fetch_assoc($stmt_search)) {
                    db2_execute($stmt_update, array($row["NUM_ORDER"] , "1". date('ymd') , $API_CODE, $row["ORDER_ID"]));
                }
            }
            $stmt_update = '';
        }
        $stmt_search = '';
    }


    public static function searchShippmentTracking()
    {
        $Orion = new Orion();
        $connexion = $Orion->getConnexion();
        // on selectionne les commandes astore qui ont un shipment_notif_sent = 0 et qui sont dans le suivi des transporteurs
        $sql_search = "SELECT trim(ORDER_ID) AS ORDER_ID, coalesce(trim(C0CKNA),'Chomette')  AS C0CKNA, trim(PRT_URLSUIVI) AS PRT_URLSUIVI from orionbd.API_ORDER_TRACKING
        inner join orionbd.PIXRFTRP on prt_cde = order_num and prt_code_client= customer_num and trim(PRT_URLSUIVI) <> ''
        left outer join orionbd.ORC0REP on C0B8CD = PRT_TRANSP and C0AMCD=2
        where  shipment_notif_sent = 0 and API_CODE='MAPA' ";

        $res = db2_prepare($connexion, $sql_search);

        $WaitingOrder = [];
        if (db2_execute($res,array())) {
            while ($row = db2_fetch_assoc($res)) {
                array_push($WaitingOrder, $row);
            }
        }
        return $WaitingOrder;
    }

    public static function findArticlesByOrderId($ORDER_ID)
    {
        $Orion = new Orion();
        $connexion = $Orion->getConnexion();
        // On recherche les articles des commandes mapa.
        $sql_search = "SELECT CONCAT('SCE' , CICART) as NArticle , CICQTE as Quantite
                       FROM orionbd.ecfeilig
                       WHERE CINLOT = 'MAPA".$ORDER_ID."' AND CICART <> '999056' ";

        $res = db2_prepare($connexion, $sql_search);

        $ArticlesOrder = [];
        if (db2_execute($res,array())) {
            while ($row = db2_fetch_assoc($res)) {
                $NArticleMAPA = ApiOrderTrackingRepository::searchFromWizaplace($row["NARTICLE"]);
                $ArticlesOrder[$NArticleMAPA] = $row["QUANTITE"];
            }
        }
        return $ArticlesOrder;
    }


    public static function searchFromWizaplace($ArticleECF){
        $ArticleMapa = False ;
        $http_client = new GuzzleHttpClient([
            'base_uri' => MAPA_WIZAPLACE,
            'verify' => false
        ]);

        $url = MAPA_WIZAPLACE."catalog/products?code=".$ArticleECF."&items_per_page=1";

        try {
            $response = $http_client->request('POST', $url, [
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Authorization' => 'token 8Q0hdXaehrYcC6kSR+N3yugxPnHs0D0i5lht+J9N'
                ]
            ]);
            $code = $response->getStatusCode();
            $body = $response->getBody()->getContents();
            if ($code === 200) {
                $body = json_decode($body, true);
                $ArticleMapa = isset($body[0]["offers"][0]["productId"]) ? $body[0]["offers"][0]["productId"] : FALSE ;
            }
        } catch (\Exception $ex) {
            $logger->error(sprintf('Send Tracking exception : commande "%s" message "%s"', $anOrderTracking["ORDER_ID"], $ex->getMessage()));

        }

        return $ArticleMapa;

    }

    public static function update($ORDER_ID){
        $Orion = new Orion();
        $connexion = $Orion->getConnexion();
        $sql_update = "UPDATE orionbd.API_ORDER_TRACKING set shipment_notif_sent = ? , UPDATEDATE = ? where API_CODE='MAPA' and ORDER_ID= ?";
        $res = db2_prepare($connexion, $sql_update);
        $update = FALSE;
        if (db2_execute($res, array(1 , "1". date('ymd') ,  $ORDER_ID  ))) {
            $update = TRUE;
        }
        return $update;
    }

}
