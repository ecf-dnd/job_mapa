<?php
/*
 *
 */
define('MODE_DEVELOPPEMENT',false);
define('MODE_PREPROD',false);
define('FICHIER_LOG', './logs/log_'.date("Y-m-d").'.log');
define('FICHIER_JSON', './inc/token.json');

if (MODE_DEVELOPPEMENT == true) {

    /*
     * Databases connexions
     */
    define("SERVER", "mysql:host=localhost");
    define("PORT", 3306);
    define("USER_BDD", "root");
    define("PWD_BDD", '');
    define("DB_NAME",'dbname=mychomette');

    /**
     * URL Connexion au webhook
     */
    define("MAPA_SRV", "http://localhost/MAPA/php_mapa_webhook/");

} elseif (MODE_PREPROD == true) {

    /*
     * Databases connexions
     */
    define("HOST", "*LOCAL");
    define("USERNAME", "gescom");
    define("PASSWORD", "gescomb");
    define("URL_API", "http://172.16.128.5:10090/gfontaine/api_ecf");

    /**
     * URL Connexion au webhook
     */
    define("MAPA_SRV", "http://localhost/MAPA/php_mapa_webhook/");

} else {

    /*
     * Databases connexions
     */
    define("HOST", "*LOCAL");
    define("USERNAME", "gescom");
    define("PASSWORD", "gescomc");
    define("URL_API", "http://172.16.128.4:10090/api_ecf");
    
    /**
     * URL Connexion au webhook
     */
    define("MAPA_SRV", "https://mapadirect.ecf-info.net/");

    define("MAPA_WIZAPLACE" , "https://marketplace.mapadirect.fr/api/v1/");



}

spl_autoload_register(function ($class_name) {
    if (file_exists('./classes/Repository/'.$class_name . '_Repository.php')) {
        require_once './classes/Repository/'.$class_name . '_Repository.php';
    }
    if (file_exists('./classes/Entity/'.$class_name . '.php')) {
        require_once './classes/Entity/'.$class_name . '.php';
    }
});